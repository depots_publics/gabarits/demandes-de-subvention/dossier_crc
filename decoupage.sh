cd soumission
# CV
pdftk ../dossier.pdf cat 1 output "cv_1_conventions.pdf"
pdftk ../dossier.pdf cat 2-3 output "cv_2_contributions_importantes.pdf"
pdftk ../dossier.pdf cat 4-9 output "cv_3_contributions_recherche.pdf"
pdftk ../dossier.pdf cat 10-11 output "cv_4_leadership.pdf"
pdftk ../dossier.pdf cat 12-15 output "cv_5_experience_formation.pdf"
pdftk ../dossier.pdf cat 16 output "cv_6_autres_contributions.pdf"

# documents à l'appui
pdftk ../dossier.pdf cat 17-23 output "documents_1_rapport_rendement.pdf"
pdftk ../dossier.pdf cat 24-33 output "documents_2_programme_recherche.pdf"
pdftk ../dossier.pdf cat 34-39 output "documents_3_qualite_etablissement.pdf"
